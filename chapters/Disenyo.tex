\chapter{Diseño}

En este capítulo se tratará la justificación del diseño de \textit{MathType for iOS}. Se entiende como diseño el paso previo a la implementación donde se detalla cómo tiene que ser ser el sistema y que provocará como consecuencia que se satisfagan los requisitos previamente descritos.

\medskip

El diseño engloba diferentes aspectos que se irán tratando durante el capítulo, debido a que requisitos como el de la importancia de la interfaz de usuario requieren un estudio a priori del diseño externo.

\medskip

Por otro lado, también existe un diseño arquitectónico o diseño interno desde el punto de vista del código para que éste cumpla con características como la mantenibilidad y reusabilidad.

\medskip

Finalmente, se hablará también del diseño físico de la aplicación para explicar dónde se ejecuta cada parte del conjunto software.

\section{Diseño externo}

Para el diseño de la interfaz se ha realizado un prototipo de la aplicación por la cual poder navegar entre las diferentes pantallas.

\medskip

En el caso de \textit{MathType for iOS} se ha tenido en cuenta diferentes criterios para el diseño. Por ejemplo, el número de elementos que se muestran al usuario. Estos han sido ordenados de mayor a menor importancia dependiendo de la frecuencia de uso.

\begin{itemize}
	\item {Editor de fórmulas matemáticas con toolbar de herramientas}
	\item {Editor de fórmulas matemáticas en modo handwriting}
	\item {Fórmulas recientes}
	\item {Detección de fórmulas en una imagen}
	\item {Ajustes}
\end{itemize}

Además, durante esta parte del diseño se han tenido en cuenta de los siguientes requisitos:

\begin{itemize}
	\item {Requisito no funcional 001: Diseño atractivo}
	\item {Requisito no funcional 002: Estilo autoritario}
	\item {Requisito no funcional 003: Usabilidad}
	\item {Requisito no funcional 005: Aprendizaje}
\end{itemize}

Pero también ha sido importante la interacción con estos elementos. Por ejemplo, el cómo se quiere poder ejecutar la aplicación y cómo se quiere importar/exportar una fórmula.

\medskip

En cuanto a la ejecución, se sabe de antemano que \textit{iOS} permite los modos conocidos como \textit{Split View} y \textit{Slide Over}. Estos lo que hacen es mostrar en pantalla varias aplicaciones al mismo tiempo, con la diferencia de que \textit{Split View} divide en dos la pantalla para mostrar a cada lado una aplicación diferente y \textit{Slide Over} superpone la vista de una de las aplicaciones sobre otra.

\medskip

Esto de cara al uso de \textit{MathType for iOS} es una buena forma de mejorar la productividad debido a que ésta no tiene sentido de existencia por sí misma, sino que es una herramienta que complementa a otras aplicaciones.

\medskip

Por la parte de importar/exportar una fórmula también se ha realizado un estudio previo y se sabe que \textit{iOS} permite implementar la funcionalidad Drag\&Drop. Ésta ayuda a eliminar elementos en pantalla que sugieran el importar o exportar, como añadir un botón, para así agarrar la imagen previsualizada y moverla a donde se quiere tener. 

\medskip

Un primer diseño final, sin tener en cuenta colores ni la guía de estilos del sistema operativo iOS ha sido el de la siguiente figura. En el apéndice \ref{MathTypeFigma} se pueden observar las capturas restantes de la aplicación.

\begin{adjustbox}{center,caption={Pantalla principal de MathType for iOS.},nofloat=figure}
	% This is a non-float method. Look more here https://tex.stackexchange.com/questions/8625/force-figure-placement-in-text.
	\includegraphics[width=\textwidth, height=\textheight, keepaspectratio]{MathType_Figma/MathType_Figma-0.jpg}
\end{adjustbox}

Los motivos del uso de este layout para la aplicación son varios:

\medskip

Primero de todo y como se ha comentado anteriormente es el uso que el usuario hace de la funcionalidad. Los usuarios de \textit{MathType for iOS} utilizan el modo manuscrito para la creación de fórmulas. Por lo tanto, que la pantalla inicial sea el modo handwriting del editor genera un aumento de la productividad y además reduce la curva de aprendizaje para utilizar la característica principal. Y como se puede ver en relación al Drag\&Drop, la previsualización en este modo se espera que pueda ser arrastrada a otras aplicaciones.

\medskip

Por otra parte nos encontramos que la edición de fórmulas, sobretodo las manuscritas, requiere de una superfície grande para sentirse cómodo, por lo que hay que optimizar el espacio. Además, hay que tener en cuenta que si se activa el modo Split View o Slide Over la superfície disminuye hasta un 70\%. Esto provoca que haya que aprovechar al máximo el espacio de trabajo.

\bigskip

\begin{adjustbox}{center,caption={MathType for iOS en modo Split View.},nofloat=figure}
	% This is a non-float method. Look more here https://tex.stackexchange.com/questions/8625/force-figure-placement-in-text.
	\includegraphics[width=0.8\textwidth, height=\textheight, keepaspectratio]{MathType_Figma/MathType_Figma-5.jpg}
\end{adjustbox}

Para resolver este problema, se ha decidido tener un menú que en principio no es visible hasta que el usuario lo necesita y lo activa mediante un gesto o posiblemente mediante un botón en una Navigation bar en la parte superior de la pantalla.

\bigskip

Finalmente, se ha decidido mantener el menú de ajustes lo más escondido posible debido a que un usuario no suele editar para cada fórmula los ajustes de la aplicación. Mantener estas opciones menos accesibles nos ayuda en puntos como: reducir la curva de aprendizaje, ya que el usuario no tiene constancia hasta más tarde que esas opciones existen; ofrecer flexibilidad, sólo si se requiere; y a mantener el menú lo más claro posible para que se puedan visualizar las \enquote*{Fórmulas recientes}.

\bigskip

\begin{adjustbox}{center,caption={Menú de MathType for iOS con las fórmulas recientes, el acceso a ajustes y cámara.},nofloat=figure}
	% This is a non-float method. Look more here https://tex.stackexchange.com/questions/8625/force-figure-placement-in-text.
	\includegraphics[width=0.5\textwidth, height=\textheight, keepaspectratio]{MathType_Figma/MathType_Figma-1.jpg}

	\hspace{2mm}

	\includegraphics[width=0.5\textwidth, height=\textheight, keepaspectratio]{MathType_Figma/MathType_Figma-2.jpg}
\end{adjustbox}

\section{Diseño interno}

\subsection{Complejidad del diseño}

Las funcionalidades que quiere llegar a ofrecer \textit{MathType for iOS} pueden ser sencillas a simple vista, sin embargo existe una complejidad escondida debido a diferentes factores como: el uso de distintos lenguajes de programación, la comunicación entre éstos o la inclusión de \textit{MathType for iOS} en el ciclo de desarrollo y testeo de la empresa \textit{Maths for More S.L.} que condicionan la arquitectura de la aplicación.

\subsubsection{Modularidad}

Para este proyecto, y como se explica en la planificación, existe un riesgo a la hora de estimar el número de horas. Con esto en mente es necesario ofrecer una solución que permita satisfacer los plazos de entrega de un producto terminado. Para ello se requiere pensar en tener un producto útil lo más pronto posible y después basarse en la idea de \textit{construcción modular} para añadir a la aplicación todas aquellas funcionalidades extras que mejoren el producto pero que no sean necesarias en sí mismas.

\medskip

Esto hace que la arquitectura requiera un bajo nivel de acoplamiento entre las diferentes componentes y así sea sencillo desactivar las que no den tiempo a terminar y que el producto siga en funcionamiento.

\subsubsection{Limitaciones del sistema iOS}

El sistema operativo donde se desarrolla la aplicación presenta limitaciones que afectan a la arquitectura del programa.

\medskip

Primero de todo la gestión de la memoria por parte del garbage collector\cite{GarbageCollectorBook} se hace mediante las llamadas referencias fuertes y referencias débiles. Esto genera que la creación de las vistas tengan que guardar en algunos casos referencias fuertes para no perder el acceso.

\medskip

Por otro lado, el sistema también se encuentra limitado en algunas funciones como las de comunicación entre aplicaciones. iOS separa la información entre aplicaciones por seguridad y eso genera una dificultad añadida para compartir información, que en el caso de MathType for iOS sería exportar e importar las fórmulas entre aplicaciones. Sin embargo, se permite usar ciertas clases como las de tipo \textit{Imagen} para sortear esta dificultad.

\medskip

Aún así, esto último supone un cambio en la forma de tratar la importación y exportación de fórmulas que ya realiza \textit{MathType Web}. Dicha estrategia lo que hace es mantener atributos con información relativa a la fórmula en formato \textit{MathML} para así saber en todo momento qué formula se muestra en la imagen. Pero en este caso no es posible, ya que no está permitido compartir atributos adicionales ni en parámetros opcionales ni en la propia imagen dado que el sistema también elimina los campos de metadatos que puedan haber.

\subsubsection{Simplicidad para cambiar las vistas}

Actualmente la empresa \textit{Maths for More S.L.} está trabajando en una nueva versión mayor de su editor MathType Web. Dicha versión implementará importantes cambios de interfaz, por lo que la arquitectura se ve condicionada de forma que debe aislar cada una de las vistas que presenta al usuario y facilitar su cambio en caso de ser necesario. De esta forma, ante un cambio repentino de interfaz es posible cambiar la vista asociada de manera simple y rápida.

\subsubsection{Comunicación entre Swift y JavaScript}

Los dos lenguajes que utiliza \textit{MathType for iOS} son Swift\cite{SwiftGithub} y JavaScript\cite{flanagan1996javascript}. Swift es el idioma principal que se utiliza para el desarrollo nativo de la aplicación. Sin embargo, debido a que uno de los requisitos es la integración y extensión de MathType Web es necesario usar JavaScript para ello.

\medskip

Esta decisión requiere entonces del uso de un sistema de comunicación entre ambos lenguajes para transferir la información entre el editor de fórmulas y la aplicación. Además del aprendizaje vinculado a ambos lenguajes.

\subsection{Arquitectura interna}

\subsubsection{Patrón MVC}

\begin{adjustbox}{center,caption={Diagrama de funcionamiento del patrón MVC.},nofloat=figure}
	% This is a non-float method. Look more here https://tex.stackexchange.com/questions/8625/force-figure-placement-in-text.
	\includegraphics[width=0.6\textwidth, height=\textheight, keepaspectratio]{Diagrama_MVC.png}
\end{adjustbox}

Debido a la complejidad de la que se ha hablado anteriormente, el patrón de diseño escogido es el de Modelo-Vista-Controlador, abreviado como MVC\cite{MVCPattern}.

\medskip

Dicho patrón mejora la extensibilidad y mantenibilidad. Sus principales ventajas para este proyecto son:

\begin{itemize}
	\item \textbf{La implementación se realiza de forma modular.} Esto ayuda al desarrollo modular comentado anteriormente. Dicha característica es una de las más importantes debido a que gracias a ella es posible añadir y quitar funcionalidades rápidamente y que el proyecto pueda seguir en funcionamiento, por lo que se hace interesante de cara al \textit{TFG}, por si la falta de tiempo sugiere que ciertas funcionalidades deben ser eliminadas; y de cara a \textit{Maths for More S.L.} por si requieren de lanzar versiones de prueba donde observar si una nueva funcionalidad es útil para sus usuarios o no. En las siguientes secciones se explica con más detalles el cómo se consigue.
	\item \textbf{La modificación de las vistas no afecta al modelo del dominio.} Esto hace que la vista tenga un acoplamiento bajo respecto al modelo y por lo tanto es más sencillo cambiar la vista por otra en caso de ser necesario, es decir, solamente la representación de los datos varía y no los datos.
	\item \textbf{La modificación del modelo solamente afecta a éste.} Esto implica que en caso de cambio en el modelo bastaría con modificar las interfaces que puedan haber.
\end{itemize}

A cambio, nos supondría tener las siguientes desventajas:

\begin{itemize}
	\item \textbf{Mayor tiempo inicial de desarrollo.} La implementación del MVC es costosa al principio, provocando que los tiempos de desarrollo iniciales aumenten. Esto afecta de manera directa a la planificación del proyecto del Capítulo \ref{ref:PlanificacionTemporal}.
	\item \textbf{Requiere de un mecanismo de eventos.} Dichos eventos serán utilizados para responder de manera específica ante situaciones concretas.
	\item \textbf{El número de clases e interfaces es mayor}. En otros patrones no son necesarias grandes cantidades de clases, esto a la larga podría llegar a provocar problemas de gestión de archivos para contener dichas clases y/o repetición de código si no se sabe de la existencia de todas las clases del sistema.
\end{itemize}

\subsubsection{Diseño estático del patrón MVC}

A continuación se explican los detalles más importantes del diseño estático del patrón MVC. Cada párrafo explica uno de los componentes (\textit{Modelo}, \textit{Vista} o \textit{Controlador}) y muestra de manera aislada el diagrama lógico asociado a dicho componente. Para ver el diagrama lógico global del sistema, hay que ir al \textit{Apéndice \ref{Diagrama_Logico_MathType}}.

\medskip

Como aclaración previa, aquellas clases que contienen prefijos \enquote*{NS}, \enquote*{UI} o \enquote*{WK} son clases de Swift y por lo tanto no se han especificado sus atributos y/o métodos, solamente se ha hecho una referencia a ellos.

\paragraph{Modelo}

\begin{adjustbox}{center,caption={Diagrama lógico del modelo.},nofloat=figure}
	% This is a non-float method. Look more here https://tex.stackexchange.com/questions/8625/force-figure-placement-in-text.
	\includegraphics[width=\textwidth, page=1, keepaspectratio]{Diagrama_Conceptual/Diagrama_logico-Split.pdf}
\end{adjustbox}

El sistema a implementar por \textit{MathType for iOS} no requiere de la gestión de ninguna base de datos. Sin embargo, el modelo se ocupará de crear la estructura interna de los objetos que se utilizan en el modelado conceptual. Esto quiere decir que en él estarán todas las clases que requieran de una semántica, como las fórmulas o el propio editor.

\medskip

El motivo de hacer esto es para crear una capa de abstracción y estandarización de las estructuras durante la comunicación entre capas. Si se pasaran primitivas se podría dar el problema de tener funciones con muchos parámetros; y si se pasaran objetos básicos (clase de la que heredan todas las clases) el mantenimiento del código se reduce ya que o bien se documenta qué propiedades y atributos tiene que cumplir dicho objeto, o se generan inconsistencias como acceder a una propiedad inexistente. Además, esto permite tratar los datos si así se requiere solamente cambiando el valor de retorno de estas clases.

\medskip

Finalmente, tanto las \textit{vistas} como los \textit{controladores} acceden al modelo y lo utilizan en sus propias llamadas, por lo que se crean relaciones de dependencia hacia estas clases del modelo. 

\paragraph{Vista}

\pagebreak
\includepdf[landscape, pages=2]{Diagrama_Conceptual/Diagrama_logico-Split.pdf}

\textit{MathType for iOS} utiliza las llamadas \enquote*{vistas} para darle una representación a los datos que serán visualizados.

\medskip

Una de las preocupaciones más grandes con dichas vistas es la mejora de su reusabilidad y modularidad. Para ello se llevan a cabo una serie de restricciones, algunas se reflejan en el diagrama lógico en forma de clases o relaciones y otras son restricciones textuales que afectan sobretodo al diagrama de secuencia de algunas de las acciones.

\medskip

Las restricciones que se han llevado a cabo son las siguientes:

\begin{itemize}
	\item \textbf{Solamente una vista puede decidir qué y cómo representar información al usuario.} Ya que como se ha dicho antes, se delega el trabajo de representación de los datos a esta capa.
	\item \textbf{Las vistas tienen referencias a interfaces en vez de a sus propios controladores.} Debido a que una vista tiene un potencial de cambio mayor que el de un controlador, se requiere una capa de abstracción como lo puede ser una interfaz para disminuir el acoplamiento.
	\item \textbf{Los límites de la vista los define la vista padre.} Esto quiere decir que una vista le proporciona un espacio visual a la subvista donde tiene permitido renderizarse. De otra manera, podríamos encontrar problemas de solapamiento de vistas y contención de las mismas. Por ejemplo si dos subvistas deciden ocupar la mitad del espacio, ya que en este caso habría que ir a cada subvista y configurarlas para ocupar sólo la mitad. En cambio, si ocupan todo el espacio de renderizado y el padre es el encargado de decidir que quiere la mitad para cada una, otra vista que tenga esas mismas subvistas puede ponerlas en la distribución que le apetezca sin crear una subclase de dichas subvistas para cambiar la configuración de cuánto espacio van a ocupar. Por lo tanto se mejora la mantenibilidad y reusabilidad del código.
	\item \textbf{Cada vista solamente puede comunicarse con su propio controlador.} Esto aumenta la cantidad de código ya que muchas veces se crean largas cadenas de llamadas. Por ejemplo, si Vista A realiza una acción que afecta a la vista B, se crea la cadena \enquote*{Vista A llama a Controlador A - Controlador A llama a Controlador B - Controlador B llama a Vista B}. Sin embargo, se crean menos dependencias entre vistas y/o controladores, por lo que con ello se consigue aislar funcionalidades. De esta manera, es posible mantener el sistema por módulos donde, desactivando un controlador de una funcionalidad específica en la clase principal, se puede eliminar dicha funcionalidad. Análogamente, si activamos un controlador nuevo y añadimos su vista, es posible añadir una nueva funcionalidad.
\end{itemize}

En cuanto al segundo punto, afecta de manera directa al diagrama lógico tal y como se ve con la clase \textit{HomeView}. Cada vista tiene una relación con su propia interfaz delegada que sigue el patrón de nombre \enquote*{[NombreDeLaVista]Delegate}, cosa que le permite obtener vistas genéricas del tipo \textit{ViewBase} para ser añadidas. De esta forma, se desacoplan las subvistas y el delegado (el controlador que implementa dicha interfaz) es quien se ocupa de la lógica.

\medskip

En cuanto a los demás puntos, se tratan de restricciones textuales y dependen de manera directa del programador, que es responsable de no incumplirlas para no romper la arquitectura del sistema.

\paragraph{Controlador} 

\pagebreak
\includepdf[landscape, pages=3]{Diagrama_Conceptual/Diagrama_logico-Split.pdf}

Dentro de la aplicación, los controladores tienen el rol de recibir los cambios del modelo y viceversa. Además, adquieren también la tarea de configuración y coordinación de eventos. Esto quiere decir que son los encargados de llamar a los otros controladores o su propia vista para entregar dichos eventos, con los cuales es posible administrar el ciclo de vida de la aplicación.

\medskip

Para ello, se hace uso de clases o interfaces apodadas \textit{Delegate}. Dichos elementos son utilizados para desacoplar de los controladores las referencias a otros objetos en los que se \enquote*{delegan} el tratamiento de sus propios eventos.

\medskip

Sin embargo, debido a la estructura de la aplicación en la que hay una clase (\textit{HomeController}) que representa la \enquote*{pantalla principal}, ésta sí necesita conocer el estado global de la aplicación y las funciones de los otros controladores, por lo que queda fuertemente acoplada con las demás clases. Pero, al ser la encargada de instanciar los demás controladores y comunicar los cambios que puedan provocar entre ellos, como consecuencia de sus acciones, se ve justificado. Por último, se cree improbable tener cambios en los eventos del sistema, así que también es improbable tener consecuencias debido al nivel de acoplamiento.

\subsubsection{Diseño dinámico del patrón MVC}

Aquí, se explican los detalles referentes al diseño dinámico de la implementación realizada del patrón MVC. La motivación de este apartado es explicar algunas de las restricciones de desarrollo que se quieren imponer en la creación y uso de los componentes \textit{Vista} y \textit{Controlador}.

\paragraph{Vista.}

Como se ha hablado durante el diseño estático la \textit{Vista}, se han incluido restricciones para mejorar la reusabilidad y mantenibilidad del código. Entre éstas, la restricción de \textit{Los límites de la vista los define la vista padre} y \textit{Cada vista solamente puede comunicarse con su propio controlador} tienen consecuencias en el \enquote*{qué se espera del desarrollador a la hora de crear y mantener \textit{MathType for iOS}}.

\medskip

Así pues, el siguiente diagrama muestra una consecuencia directa de estas restricciones en la forma de actuar del sistema. Sin embargo, hay que tener en cuenta que no se incluyen los diagramas de las subvistas, ya que sería un ejemplo análogo y por lo tanto no es relevante para esta demostración. Además, se han obviado los diagramas de llamadas como \textit{addSubView} por tener nombres autoexplicativos.

\pagebreak

\begin{adjustbox}{center,caption={Diagrama de secuencia de la vista HomeView.},nofloat=figure}
	% This is a non-float method. Look more here https://tex.stackexchange.com/questions/8625/force-figure-placement-in-text.
	\includegraphics[width=\textwidth, height=0.95\textheight, keepaspectratio]{Diagrama_Secuencia/Diagrama_Secuencia-Views_secuencia.png}
\end{adjustbox}

\pagebreak

La secuencia muestra como la vista padre \textit{HomeView} utiliza una llamada a la referencia de la interfaz que implementa su controlador. A partir de aquí el controlador se hace cargo de la lógica de saber a qué otros controladores debe llamar y éstos en devolver la vista correcta.

\medskip

Esta vista, como se puede ver en el diagrama lógico, es de la superclase \textit{ViewBase}. Esto se hace para ocultar el tipo específico que finalmente la vista \textit{HomeView} inserta, desacoplando así las subvistas específicas tanto de los controladores que actúan de intermediario durante la cadena de llamadas como de las vistas padre.

\medskip

A su vez y como se comentaban en las restricciones que se han impuesto anteriormente, la vista padre en ningún momento modifica su zona de renderizado, sino que solamente tiene permitido modificar las de las subvistas propias (momento en el que llama a la función \textit{addConstraints(...)}) dentro del espacio asignado para sí misma.

\medskip

Finalmente, también hay que comentar la llamada a \textit{render()}. Esta llamada permite a las vistas tener el poder de decisión de cuándo la subvista va a ser inicializada. Esto se debe a que hay momentos en el que no interesa cargar una vista hasta un cierto momento y es mejor esperar a que el elemento vaya a ser utilizado.

\paragraph{Controlador.}

Para terminar de explicar el diseño dinámico de \textit{MathType for iOS} a continuación se muestra el comportamiento que existe cuando se da un evento y éste se propaga por las diferentes clases del sistema gracias a los eventos manejados por los controladores. En concreto, el caso de uso en el que hay que actualizar las fórmulas recientes debido a que el usuario ha introducido una fórmula nueva en la vista donde se encuentra el editor.

\pagebreak
\includepdf[landscape]{Diagrama_Secuencia/Diagrama_Secuencia-Controllers_secuencia.pdf}

Lo más destacable de esta secuencia es, que debido a que los eventos se provocan en el \textit{WebView}, es éste quien avisa a la vista. Una vez hecho esto la vista delega la responsabilidad de tratar el evento a su \textit{delegado}, que en este caso sería \textit{BrowserController} (que ha implementado la interfaz \textit{BrowserViewDelegate}). Entonces, éste notifica al controlador de la vista padre el evento.

\medskip

Llegados a este punto, \textit{HomeController} decide que dicho evento requiere ser redirigido al controlador \textit{MenuController}, el cual aplica la decisión que ante dicho evento es necesario actualizar las fórmulas recientes que son mostradas en su vista.

\subsubsection{Puente de comunicación Swift-JavaScript}

Como se explica en el alcance de \textit{MathType for iOS}, por temas de reusabilidad y mantenibilidad de código se quiere integrar la versión web de su editor en el sistema. La versión web de \textit{MathType} (o \textit{MathType Web}) se ejecuta en el lenguaje \textit{JavaScript}.

\medskip

El sistema operativo iOS permite añadir una vista llamada \textit{WebView}\cite{WKWebViewApple} con la que visualizar contenido web como si de un navegador se tratara. \textit{MathType for iOS} incluye esta vista para así tener la posibilidad de crear un contexto donde ejecutar \textit{MathType Web}.

\medskip

Este contexto de ejecución es donde se encuentra una instancia de \textit{Editor} y además proporciona un método de comunicación entre \textit{Swift} y \textit{JavaScript} mediante inyección de código.

\medskip

Para la comunicación se establece la interfaz \textit{JavascriptCommunicationProtocol}, tal y como se muestra en el diagrama lógico, que la vista contenedora del \textit{WebView} tiene que implementar. Dicha interfaz establece un protocolo para gestionar la inyección de código y objetos en el contexto de JavaScript, permitiendo así evaluar código JavaScript desde Swift y crear objetos JavaScript que invoquen código de Swift.

\medskip

Para acabar, el código JavaScript gestiona su ciclo de vida mediante eventos, que son llamadas a partes específicas del código cuando ocurren situaciones concretas como un cambio en los estilos mostrados o un click sobre un elemento\cite{EventsMDN}. Dichos eventos son lanzados tanto por los cambios que se producen en el contexto de ejecución de \textit{JavaScript} como en el de \textit{Swift}. Varios ejemplos de eventos son: a nivel de \textit{JavaScript}, la comunicación hacia \textit{Swift} de que una nueva fórmula ha sido creada y por lo tanto debe insertar la que había anteriormente como \enquote*{fórmula reciente}; y a nivel de \textit{Swift}, la elección por parte del usuario de una toolbar específica para el editor.

\medskip

El principal motivo que justifica el uso de los eventos como método de gestión del ciclo de vida en \textit{JavaScript} es la naturaleza de esta tecnología, que gira entorno al uso de eventos. Existen eventos de muchos tipos que permiten ejecutar partes de código de manera paralela con relativa facilidad. Por lo tanto, disminuye los errores y aumenta la simplicidad del código, cosa que implica un aumento de la mantenibilidad.

\subsubsection{Uso del patrón estrategia en las opciones de configuración}

Debido a que las opciones de configuración son presentadas en una tabla donde el usuario puede escoger un valor, existe un comportamiento común. Por un lado siempre hay un texto descriptivo de la opción que se quiere cambiar y por otro lado un elemento de \textit{input} para permitir al usuario insertar datos, que puede ser de diferentes tipos.

\medskip

Para mejorar la reutilización del código, se ha implementado un patrón estrategia\cite{PatternsBookStrategy} en la clase \textit{ConfigurationOption} devolviendo una clase común al tipo de elemento que se quiere mostrar y que es propia del sistema en \textit{iOS} (\textit{UIViewController}). Éste permite variar el tipo de elemento utilizado para que el usuario inserte información. Por ejemplo, en el caso del idioma el elemento adquiere la forma de una lista donde le es posible seleccionar qué idioma quiere. Mientras que en una opción como la resolución de imagen que se quiere, podría ser una casilla donde insertar un valor numérico entre 0 y 100.

\medskip

Además, también se ha utilizado la clase \textit{ConfigurationOption} para aplicar el patrón factoría\cite{PatternsBookFactory}. Permitiendo facilitar la instanciación de los objetos sin la necesidad de exponer la lógica de su creación.

\medskip

Finalmente, como comentario, hay que destacar el atributo estático \textit{keepPickersViews} de \textit{ConfigurationOption} que se puede ver en el diseño lógico. Este atributo permie solucionar un problema presente en \textit{iOS} referente al uso del patrón factoría. Ya que la creación de instancias haciendo uso de este patrón genera referencias débiles de los objetos creados, por lo que son borrados por el \textit{garbage collector} a pesar de estar siendo usados. Dicha variable mantiene en memoria los objetos.

\section{Diseño físico}

Terminando ya de explicar el diseño de \textit{MathType for iOS}, falta detallar el diseño físico que utiliza.

\medskip

\textit{MathType for iOS} es una aplicación que se ejecuta de manera local, sin embargo, realiza llamadas externas a servicios de \textit{Maths for More S.L.}. Esto se debe a que funcionalidades como el reconocimiento de fórmulas o el propio archivo que contiene el editor \textit{MathType Web} que se quiere integrar en este trabajo son dependencias que se obtienen a través de llamadas externas.

\medskip

Debido a ello y a que solamente se comunica con un servidor, el diseño físico coincide con la arquitectura \textit{cliente-servidor}\cite{DefClienteServidor}.

\medskip

Este tipo de arquitectura realiza un procesamiento cooperativo donde cada uno de los componentes pide servicios a otro.

\vspace{5mm}

\begin{adjustbox}{center,caption={Diagrama físico de \textit{MathType for iOS}.},nofloat=figure}
	% This is a non-float method. Look more here https://tex.stackexchange.com/questions/8625/force-figure-placement-in-text.
	\includegraphics[width=\textwidth, height=0.95\textheight, keepaspectratio]{Diagrama_Fisico/Diagrama_Fisico.png}
\end{adjustbox}

Como se puede ver en la figura anterior, la aplicación realiza peticiones al servidor. De esta forma delega el trabajo más complejo y que requiere de mayor poder de cómputo y recibe un resultado que posteriormente trata.

\medskip

Las principales ventajas de utilizar este sistema son: delegar un trabajo de cómputo pesado a una máquina con mayor potencia y así acelerar las operaciones y la escalabilidad, ya que se puede aumentar tanto la capacidad del cliente como la del servidor por separado.

\medskip

Sin embargo, supone a su vez tener desventajas, como la dependencia de un servicio externo por el cual es obligatorio tener una conexión a Internet; también los problemas de latencia de la red, que pueden perjudicar a la velocidad de respuesta; y por último la sobresaturación de los servidores, en caso de que muchos clientes realicen peticiones al mismo tiempo hacia un mismo servidor.