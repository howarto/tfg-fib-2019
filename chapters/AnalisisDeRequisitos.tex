\chapter{Análisis de requisitos}
El análisis de requisitos es una de las partes más importantes de un proyecto software. Esto es debido a su impacto directo en la elección de las características que debe tener el sistema a construir y que con las cuales se cumplirán dichos requisitos, alcanzando así los objetivos propuestos.

\medskip

Existen diferentes definiciones de qué es un \textit{requisito software}, como por ejemplo la que contiene el \textit{Glosario IEEE de Términos de Ingeniería del Software} y la definición que se encuentra en \textit{La Guía SWEBOK}.

\bigskip

El \textit{Glosario IEEE de Términos de Ingeniería del Software}\cite{IEEEStandardRequisiteDefinition} define \textit{requisito software} como:
\begin{quote}
	\begin{itemize}
		\item Condición o capacidad requerida por un usuario para solucionar un problema o conseguir un objetivo.
		\item Condición o capacidad que tiene que ser cumplida por un sistema o un component de un sistema para satisfacer un contrato, estándar, especificación o cualquier otro documento formal.
		\item Representación documentada sobre una condición o capacidad como 1 o 2.
	\end{itemize}
\end{quote}

\bigskip

Por otra parte, La Guía SWEBOK define requisito software como:
\begin{quote}
	Un requisito software es la propiedad que un software desarrollado o adaptado debe tener para resolver un problema concreto.\cite{SWEBOKRequisiteDefinition}
\end{quote}

Además, los requisitos se pueden categorizar en requisitos funcionales y en requisitos no funcionales.

\medskip

\textit{La Guía SWEBOK} define los requisitos funcionales como:
\begin{quote}
	Un requisito funcional especifica una función que un sistema o componente de un sistema debe ser capaz de llevar a cabo.\cite{SWEBOKFunctionalAndNonFunctionalRequisiteDefinition}
\end{quote}

Mientras que define los requisitos no funcionales como:
\begin{quote}
	Los requisitos no funcionales son aquellos que especifican aspectos técnicos que debe incluir el sistema, y que pueden clasificarse en restricciones y calidades.\cite{SWEBOKFunctionalAndNonFunctionalRequisiteDefinition}
\end{quote}

Entonces, debido a la importancia de dichos requisitos, en las siguientes dos secciones se mostrarán los diferentes requisitos de la aplicación MathType for iOS así como la justificación de cada requisito.

\section{Requisitos funcionales}

En esta sección se expondrán los diferentes requisitos funcionales que requiere \textit{MathType for iOS}. En cada uno de los requisitos se les dará una enumeración junto a un nombre. Posteriormente se hará una descripción detallada del caso de uso junto a una justificación del porqué es necesario dicho caso de uso. Finalmente, se explicará el criterio de satisfacción que se debe cumplir para poder
 dar por satisfecho el caso de uso.

\bigskip

\textbf{Requisito funcional 001: Creación/Edición de imágenes de fórmulas matemáticas.}\\
\textbf{Descripción:}\ \textit{\gls{MathTypeEditor} for iOS} permitirá la creación y edición de fórmulas matemáticas en el sistema operativo iOS.\\
\textbf{Justificación:}\ Los usuarios usan \textit{MathType for iOS} para crear y/o editar fórmulas matemáticas, por lo que poder utilizar MathType dentro de la aplicación pueden hacerlo.\\
\textbf{Criterio de satisfacción:}\ Los usuarios pueden utilizar todas las funcionalidades que ofrece MathType para webs.

\bigskip

\textbf{Requisito funcional 002: Exportar fórmulas desde \textit{MathType for iOS} a otra aplicación.}\\
\textbf{Descripción:}\ \textit{MathType for iOS} podrá exportar las fórmulas creadas como imágenes a otras aplicaciones que permitan insertar imágenes.\\
\textbf{Justificación:}\ Los usuarios de \textit{MathType for iOS} quieren crear fórmulas para insertarlas en otras aplicaciones. Por lo tanto, este caso de uso les permite exportar las formulas que han creado.\\
\textbf{Criterio de satisfacción:}\ El usuario puede exportar de \textit{MathType for iOS} a una aplicación diferente que acepta imágenes de terceras aplicaciones.

\bigskip

\textbf{Requisito funcional 003: Importar desde otra aplicación a \textit{MathType for iOS}.}\\
\textbf{Descripción:}\ \textit{MathType for iOS} permitirá importar imágenes con fórmulas de otras aplicaciones para editarlas.\\
\textbf{Justificación:}\ Los usuarios de \textit{MathType for iOS} crean fórmulas para insertarlas en otras aplicaciones. Por lo tanto, este requisito permite al usuario importar esas fórmulas de nuevo para así editarlas y no tener que crearla desde cero.\\
\textbf{Criterio de satisfacción:}\ El usuario puede importar desde \textit{MathType for iOS} una fórmula desde una aplicación diferente.

\bigskip

\textbf{Requisito funcional 004: Cambio de formato de imagen \footnote{El requisito funcional estaba contemplado durante el diseño y la toma de requisitos de la aplicación. Sin embargo, debido a limitaciones del sistema, se ha eliminado. En los capítulos de implementación y planificación se explica con más detalles el porqué de su eliminación.}.}\\
\textbf{Descripción:}\ \textit{MathType for iOS} permitirá la elección del formato de imagen (PNG o SVG) de las fórmulas creadas.\\
\textbf{Justificación:}\ Las diferentes aplicaciones existentes no soportan cualquier tipo de formato de imagen. Permitir elegir entre los dos formatos más utilizados y con mejores características técnicas ayuda al usuario a obtener el mejor resultado posible dependiendo de qué formato de imagen soporte el software al que quiere exportar las imágenes.\\
\textbf{Criterio de satisfacción:}\ El usuario puede obtener imágenes ya sea en formato PNG o SVG.

\bigskip

\textbf{Requisito funcional 005: Detección de fórmula en imagen.}\\
\textbf{Descripción:}\ \textit{MathType for iOS} permitirá tomar una fotografía con la cámara del móvil o desde la biblioteca de fotos del mismo móvil y reconocer la fórmula para posteriormente poder visualizarla y/o editarla.\\
\textbf{Justificación:}\ Muchas veces los usuarios son profesores que utilizan libros físicos de matemáticas donde encuentran fórmulas que les gustaría utilizar en sus papers o en los tests para sus alumnos. Esta funcionalidad les da una facilidad extra a la hora de hacerlo.\\
\textbf{Criterio de satisfacción:}\ El usuario puede realizar o seleccionar una imagen con una fórmula no manuscrita para ser procesada y obtener así la fórmula para ser editada en \textit{MathType for iOS}.

\bigskip

\textbf{Requisito funcional 006: Mostrar fórmulas recientes.}\\
\textbf{Descripción:}\ \textit{MathType for iOS} permitirá consultar las últimas fórmulas creadas y/o editadas.\\
\textbf{Justificación:}\ Hay veces que los usuarios quieren editar una fórmula que han creado recientemente pero debido a que la han borrado tienen que volver a crear la fórmula desde cero. Para mejorar la experiencia de uso se pueden mostrar las fórmulas recientes y permitir así editar desde su última edición.\\
\textbf{Criterio de satisfacción:}\ El usuario puede crear una fórmula, borrar la fórmula, crear otra nueva y entonces poder seleccionar la fórmula antigua para ser editada de nuevo desde el último momento antes de borrarla.

\bigskip

\textbf{Requisito funcional 007: Seleccionar fórmula reciente.}\\
\textbf{Descripción:}\ MathType for iOS permitirá editar una fórmula creada recientemente.\\
\textbf{Justificación:}\ El usuario habitualmente quiere volver a editar una fórmula creada recientemente para modificarla y así ahorrar tiempo al no tener que crearla de nuevo desde el principio.\\
\textbf{Criterio de satisfacción:}\ El sistema puede enviar notificaciones informativas al usuario.


\bigskip

\textbf{Requisito funcional 008: Recibir notificaciones informativas.}\\
\textbf{Descripción:}\ \textit{MathType for iOS} permitirá la creación de diálogos informativos al usuario para explicar errores que hayan podido surgir durante su ejecución.\\
\textbf{Justificación:}\ Existen situaciones donde hay que mantener informado al usuario del estado del sistema. Por ejemplo, si ha habido un error.\\
\textbf{Criterio de satisfacción:}\ El sistema puede enviar notificaciones informativas al usuario.

\bigskip

\textbf{Requisito funcional 009: Cambio de toolbar.}\\
\textbf{Descripción:}\ \textit{MathType for iOS} permitirá escoger qué barra de herramientas se muestra en el editor de entre las ya ofrecidas por \textit{MathType Web}: normal, simple y chemistry.\\
\textbf{Justificación:}\ Cada barra de herramientas hace que ciertas herramientas sean más accesibles, dependiendo de qué fórmulas realice el usuario le conviene una u otra para ir más rápido.\\
\textbf{Criterio de satisfacción:}\ El usuario puede escoger una barra de herramientas y el sistema es capaz de cambiarla.

\bigskip

\textbf{Requisito funcional 010: Cambio de idioma.}\\
\textbf{Descripción:}\ \textit{MathType for iOS} permitirá escoger en qué idioma está la aplicación.\\
\textbf{Justificación:}\ El usuario se siente más cómodo usando la lengua que él prefiere o entiende.\\
\textbf{Criterio de satisfacción:}\ El usuario puede escoger en qué lengua está el sistema y el sistema es capaz de adaptarse a dicha elección cambiando los textos al idioma elegido.

\section{Requisitos no funcionales}

Para terminar, detallaremos los requisitos no funcionales de \textit{MathType for iOS}. Para este tipo de definición se ha utilizado una plantilla que separa los diferentes requisitos no funcionales por categorías explicando el nombre del requisito, la categoría que se le asigna dentro de The Volere Requirements Specification Template\cite{VolereRequirements}, la descripción detallada de cada requisito, la justificación de por qué es necesario dicho requisito y finalmente la condición de satisfacción que se tiene que cumplir para dar por alcanzado el requisito.

\subsection{Requisitos de interfaz \enquote*{Look And Feel}}

\textbf{Requisito no funcional 001:\ Diseño atractivo}\\
\textbf{Según Volere: }\ 10a\\
\textbf{Descripción:}\ El sistema tiene que ser atractivo para los usuarios.\\
\textbf{Justificación: }\ Un diseño que gusta hace crecer el número de usuarios.\\
\textbf{Condición de satisfacción:}\ Se toman personas independientes al proyecto y se hace una prueba de la aplicación. Si el 70\% o más de las valoraciones son positivas, se considera que el requisito ha sido satisfecho.

\bigskip

\textbf{Requisito no funcional 002:\ Estilo autoritario}\\
\textbf{Según Volere: }\ 10b\\
\textbf{Descripción:}\ El sistema tiene que seguir los estilos de diseño adecuados por los cuales dar una sensación de fiabilidad al ser utilizada.\\
\textbf{Justificación: }\ Hace falta obtener un diseño por el cual los usuarios no se sientan obligados a buscar otras aplicaciones debido al mal diseño de esta.\\
\textbf{Condición de satisfacción:}\ Se toman personas independientes al proyecto y se hace una prueba de la aplicación. Si el 70\% o más de las valoraciones son positivas, se considera que el requisito ha sido satisfecho.

\subsection{Requisitos de usabilidad y humanidad}

\textbf{Requisito no funcional 003:\ Usabilidad}\\
\textbf{Según Volere: }\ 11a\\
\textbf{Descripción:}\ El sistema tiene que ser intuitivo.\\
\textbf{Justificación: }\ El sistema tiene que seguir los estándares de diseño a los que los usuarios están acostumbrados con tal de permitir que sea fácil para ellos utilizar el sistema.\\
\textbf{Condición de satisfacción:}\ Se toman personas independientes al proyecto y se hace una prueba de la aplicación. Si el 70\% o más de las valoraciones son positivas, se considera que el requisito ha sido satisfecho.

\bigskip

\textbf{Requisito no funcional 004:\ Lengua}\\
\textbf{Según Volere: }\ 11b\\
\textbf{Descripción:}\ El sistema tiene que permitir visualizar la información de la aplicación en el idioma del dispositivo. Si el idioma no estuviera disponible, por defecto se usa el inglés.\\
\textbf{Justificación: }\ La adaptación a diferentes lenguas provoca que más usuarios puedan utilizar el sistema.\\
\textbf{Condición de satisfacción:}\ El sistema muestra la información en el idioma del dispositivo.

\bigskip

\textbf{Requisito no funcional 005:\ Aprendizaje}\\
\textbf{Según Volere: }\ 11c\\
\textbf{Descripción:}\ El sistema tiene que permitir que los usuarios utilicen el sistema sin tener formación previa.\\
\textbf{Justificación: }\ Saber utilizar el sistema sin formación previa provoca que el usuario empiece a utilizar el sistema más rápido y que cometa menos errores.\\
\textbf{Condición de satisfacción:}\ Se toman personas independientes al proyecto y se hace una prueba de la aplicación. Si los usuarios pueden crear una fórmula y arrastrarla a otra aplicación en menos de 5min, se considera que el requisito ha sido satisfecho.

\subsection{Requisitos de rendimiento}

\textbf{Requisito no funcional 006:\ Velocidad y latencia}\\
\textbf{Según Volere: }\ 12a\\
\textbf{Descripción:}\ El sistema tiene que invertir la menor cantidad de tiempo en la comunicación con los servicios externos.\\
\textbf{Justificación: }\ Un gasto menor en la cantidad de tiempo que lleva la comunicación con servicios externos genera una sensación de eficiencia hacia el usuario debido a que el tiempo de respuesta disminuye.\\
\textbf{Condición de satisfacción:}\ Se realizan 10 pruebas de escritura (handwriting) en la integración de MathType de la fórmula $x=\frac{-b\pm\sqrt{b^2-4ac}}{2a}$. Si ninguna prueba sobrepasa los 10s, se da por satisfecho el requisito.

\bigskip

\textbf{Requisito no funcional 007:\ Disponibilidad}\\
\textbf{Según Volere: }\ 12d\\
\textbf{Descripción:}\ El sistema tiene que estar disponible y funcional el mayor tiempo posible.\\
\textbf{Justificación: }\ Mantener un sistema funcionando en todo momento aumenta la confianza del usuario hacia el sistema.\\
\textbf{Condición de satisfacción:}\ El sistema en ningún momento puede estar más de 24h inutilizado.

\subsection{Requisitos operacionales y de entorno}

\textbf{Requisito no funcional 008:\ Sistemas Adyacentes}\\
\textbf{Según Volere: }\ 13c\\
\textbf{Descripción:}\ El sistema tiene que poder trabajar correctamente con diferentes servicios adyacentes.\\
\textbf{Justificación: }\ El sistema integra MathType, software que depende directamente de servicios externos. Si el sistema no logra comunicarse de forma correcta con los servicios externos quedará inutilizado.\\
\textbf{Condición de satisfacción:}\ Es posible crear la imagen de una fórmula introducida.

\subsection{Requisitos de mantenimiento y soporte}

\textbf{Requisito no funcional 009:\ Mantenimiento}\\
\textbf{Según Volere: }\ 14a\\
\textbf{Descripción:}\ El sistema tiene que poder actualizarse dependiendo del feedback de los usuarios, los bugs encontrados o posibles nuevas funcionalidades.\\
\textbf{Justificación: }\ El sistema integra MathType, un software en contínuo desarrollo con actualizaciones mensuales. Además, el sistema operativa para el que se está desarrollando, iOS, también está en contínuo desarrollo.

Esto provoca que el sistema tenga que actualizarse cada cierto tiempo para adaptarse a los nuevos cambios de sus dependencias.\\
\textbf{Condición de satisfacción:}\ Se pueden implementar nuevas features o arreglar bugs encontrados en un tiempo igual o menos a un mes.

Además, debe contar con un build y tests automatizados haciendo uso del software Jenkins.

\bigskip

\textbf{Requisito no funcional 010:\ Adaptabilidad}\\
\textbf{Según Volere: }\ 14c\\
\textbf{Descripción:}\ El sistema tiene que poder ser ejecutado y ser 100\% funcional en dispositivos iPad con una versión 12 de iOS como mínimo y ser compilado con Xcode 10 o posterior.\\
\textbf{Justificación: }\ La ejecución del sistema en dispositivos con las últimas versiones del sistema operativo iOS aumenta el número máximo de potenciales usuarios.\\
\textbf{Condición de satisfacción:}\ La aplicación es compatible y 100\% funcional desde iOS 12.0 hasta la última versión estable publicada.
