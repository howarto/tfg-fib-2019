\chapter{Sostenibilidad y compromiso social}

Para identificar la sostenibilidad del proyecto, se evalúa el impacto de tres aspectos del mismo: económico, ambiental y social. A continuación se explica cada aspecto en el contexto del TFG.

\section{Dimensión económica}

Se ha realizado un análisis de costes preciso para el proyecto teniendo en cuenta todo tipo de costes: directos, indirectos, contingencias y costes debido a incidentes. Además, dicho análisis se ha realizado respecto a las fases y tareas, por lo que su seguimiento y control es más preciso al ser un intervalo de tiempo y conjunto de recursos más acotado. Por esto mismo, se considera que la estimación de costes ha sido razonable.

\medskip

Por otra parte, este proyecto se ve respaldado con cifras internas de la empresa \textit{Maths for More S.L.} . Dichas cifras prevén que más de 500 licencias del producto serán vendidas a usuarios activos de \textit{MathType for iOS}. Por lo tanto, con un coste por licencia de aproximadamente 50\euro\ se obtiene que la cifra de retorno es de 25.000\euro\ antes de impuestos, superando así el presupuesto del proyecto. También hay que tener en cuenta que la suscripción será pagada más de una vez ya que es anual, por lo que el ingreso aún será mayor.

\medskip

Sin embargo, dicha previsión se espera de aquí a 1-2 años, por lo que \textit{Maths for More S.L.} ha decidido destinar los recursos de una sola persona para el desarrollo.

\medskip

En cuanto a otras soluciones alternativas, existen problemas debido a una falta de recursos. Son pequeñas empresas, que se dedican solamente a la edición de fórmulas matemáticas; o grandes empresas, donde la edición de fórmulas matemáticas es solamente una funcionalidad más de su programa y por lo tanto destinan una cantidad de recursos muy limitada. En ambos casos la estimación de recursos se realiza en función de los recursos humanos necesarios y orden de importancia.

\medskip

A diferencia de estas, \textit{Maths for More S.L.} y su producto \textit{MathType} se dedica específicamente a la edición de fórmulas matemáticas y tiene los recursos suficientes para desarrollar varios proyectos al mismo tiempo, pudiendo reducir el coste material reutilizando gran parte de los recursos.

\medskip

También hace falta recordar el bajo riesgo que existe en la dimensión económica debido a que, como ya se ve en la planificación, los costes por incidentes y sus respectivas probabilidades son muy bajos.

\medskip

Y ya para terminar, se quiere calcular el coste de vida útil del producto asumiendo que para darle mantenimiento se aplica un 10\% de los costes indirectos que se tienen durante todo un año. La justificación de por qué un 10\% es que la automatización de \textit{MathType for iOS} hace que el tiempo de los desarrolladores para su mantenimiento se reduzca considerablemente. Además, también durante el tiempo que se le da mantenimiento se está realizando un esfuerzo en la producción de otros productos de \textit{Maths for More S.L.}. Por lo tanto queda que de los 541,01\euro\ de los costes indirectos cada 4 meses, se gastarían 162,303\euro\ al año.

\section{Dimensión ambiental}

Durante el proyecto, se ha tenido en cuenta la reutilización de recursos. Un ejemplo ha sido el ordenador portátil o el iPad Pro, utilizados en diferentes proyectos. Sin embargo, no se ha podido hacer lo mismo para el caso del Mac Mini y la electricidad consumida.

\medskip

Se prevé que la electricidad consumida por estos equipos\cite{MacMiniConsumoWebsite}\cite{DellConsumoWebsite}\cite{DellConsumoWebsite2}\cite{iPadProConsumoWebsite} sea de 37,5kW·h de media. Por otro lado,	no se tiene en cuenta los gastos generales.

\medskip

Además, la solución proporcionada mejora entre otras cosas la rapidez respecto a otras soluciones similares. Esto provoca una reducción del consumo, ya que los usuarios utilizan menos tiempo su dispositivo al haber terminado antes el trabajo. Una reducción del tiempo de uso de los usuarios de un 15\% supone una disminución de energía consumida en el iPad Pro de 11,5kW·h a 9,775kW·h (Sin tener en cuenta pérdidas).

\medskip

Las soluciones alternativas no tienen en cuenta la eficiencia energética y la interfaz que presentan, con colores vivos y más lentas de utilizar, gastan más energía.

\vspace{5mm}

\begin{minipage}{\linewidth}
	\captionof{table}{Energía eléctrica consumida.}
	\begin{adjustbox}{center,nofloat=figure}

		% This is a non-float method. Look more here https://tex.stackexchange.com/questions/8625/force-figure-placement-in-text.
		\includegraphics[width=0.6\textwidth,keepaspectratio]{Tabla_Energia_Consumida.png}
	\end{adjustbox}
\end{minipage}

\vspace{5mm}

Además, dichos costes ambientales tienen un riesgo de aumentar un 15\% debido a que es el porcentaje máximo de desviación del proyecto calculado durante la planificación.

\medskip

Y ya para acabar, se ha utilizado en el cálculo de la huella ambiental durante la vida útil de la misma forma que en la dimensión económica, el 10\% de gasto realizado durante la realización del proyecto en un año. Por ello, si se consumen 37,54kW·h cada 4 meses, el resultado es que durante la vida útil se gastaría 11,262kW·h por año.

\section{Dimensión social}

No existe ningún editor específico de fórmulas matemáticas en iOS, pero según estadísticas internas existen usuarios de \textit{MathType} que les gustaría poder trabajar en dicha plataforma. Esta solución aporta en el ámbito social una mejora en el trabajo de las personas que necesiten insertar o editar fórmulas matemáticas.

\medskip

Las alternativas ofrecen una mala experiencia de usuario mientras que ésta tiene una mejor interfaz, provocando que el proceso de insertar o editar una fórmula sea menos tedioso y más rápido. Por lo tanto los usuarios serán más eficientes y disminuirán el tiempo dedicado a un trabajo poco gratificante.

\medskip

Además, este proyecto aporta experiencia al estudiante. Tiene la posibilidad de aplicar sus conocimientos en un entorno nuevo para él y gracias a que es un proyecto \textit{open source}, puede mostrarlo como ejemplo de su buen trabajo a empresas que piensen contratarle.

\medskip

Finalmente, los riesgos que pueden suponer un efecto negativo no existen. Únicamente se podría dar el caso en el que la competencia mejorara lo ya creado por este proyecto y por lo tanto que \textit{MathType for iOS} no fuera utilizado.

\section{Matriz de sostenibilidad}

Finalmente, se obtiene la siguiente matriz de sostenibilidad. Ésta muestra los valores en las unidades correspondientes de los apartados PPP y vida útil. Por otra parte, también se le asigna una puntuación de 0 a 10 en el caso de la dimensión social. La justificación de los valores se explican con detalle en los apartados anteriores.

\medskip

\begin{table}[H]
	\captionof{table}{Matriz de sostenibilidad.}
	\begin{tabular}{lcccl}
		& \textbf{PPP} & \textbf{Vida útil} & \textbf{Riesgos}                                                                            &  \\
		\textbf{Económica} & 19.263,4\euro    & 162,303\euro/año        & \begin{tabular}[c]{@{}c@{}}30\% costo mano de obra\\ \\ 5\% rotura de material\end{tabular} &  \\
		\textbf{Ambiental} & 37,54kW·h    & 11,262/año         & 15\%                                                                                        &  \\
		\textbf{Social}    & 8            & 8                  & Pocos                                                                                     & 
	\end{tabular}
\end{table}