\chapter{Implementación y puesta en producción} \label{capImplementacion}

Debido a que el diseño puede contener errores o no ser del todo exhaustivo, existe la posibilidad que durante la implementación del software se encuentren más limitaciones de las esperadas y por lo tanto problemas que no se habían tenido en cuenta previamente, por este motivo este capítulo explica los problemas que han ido apareciendo a lo largo del proyecto.

\medskip

Además, también se explica la automatización implementada para \textit{MathType for iOS}. Ésta, genera los paquetes finales que serán distribuidos a los clientes, verifica su calidad y se alinea la producción con los demás proyectos de la compañía \textit{Maths for More S.L.} haciendo uso de los mismos estándares.

\medskip

Y por último, es posible acceder al código software de \textit{MathType for iOS} del cual se hace referencia \href{https://bitbucket.org/howarto/tfg-fib-2019-develop}{{\color{blue} aquí}}.

\section{Dificultades encontradas}

\subsection{Cambios en el diseño visual}
Durante la etapa de diseño se explica el cómo tiene que ser visualmente \textit{MathType for iOS}. Sin embargo, no se ha tenido totalmente en cuenta la accesibilidad de algunas interacciones ni los colores corporativos entre otras cosas, por lo que ha sufrido una serie de cambios de cara a la implementación.

\medskip

Los cambios principales que se han realizado después de probar la navegación han sido: añadir una barra de navegación en la parte superior de la aplicación, para así mostrar al usuario que existe una interacción, dado que en dicha barra hay un botón que al ser pulsado despliega el menú; y la incorporación de los colores corporativos.

\medskip

Por otra parte, también hay cambio de estilos a nivel de \textit{JavaScript}. Los estilos propios que se muestran en el \textit{WebView} permiten que el editor modifique su tamaño para adaptarse a la interfaz y así poder permitir los modos \textit{Split View} y \textit{Slide Over}. Además, también permiten mostrar las fórmulas realizadas en el modo de handwriting ampliadas, para que de esta forma sea más fácil seleccionarlas y arrastrarlas donde sea necesario.

\begin{adjustbox}{center,caption={MathType for iOS en modo handwriting y a la derecha la misma pantalla con el menú desplegado. En el apéndice \ref{MathTypeApp} se pueden observar mas imágenes.},nofloat=figure}
	% This is a non-float method. Look more here https://tex.stackexchange.com/questions/8625/force-figure-placement-in-text.
	\includegraphics[width=0.5\textwidth, height=\textheight, keepaspectratio]{MathType_App/ipad_pro-homeview_hand.png}
	
	\hspace{2mm}
	
	\includegraphics[width=0.5\textwidth, height=\textheight, keepaspectratio]{MathType_App/ipad_pro-menuview_hand.png}
\end{adjustbox}

\subsection{Constraints visuales}

Uno de los requisitos es que \textit{MathType for iOS} sea compatible con múltiples dispositivos, ya que iOS es ejecutado por varios. Dichos dispositivos presentan diferencias que afectan al desarrollo visual, principalmente el tamaño de pantalla.

\medskip

Dependiendo del tamaño pueden haber elementos que no son visualizados correctamente. Sin embargo, el análisis no tiene en cuenta la forma correcta de posicionar los elementos de la interfaz para adaptarse al tamaño de pantalla. Esto ha supuesto que durante la implementación se ha tenido que estudiar el cómo hacer esto. Al principio se usaron tamaños estáticos dependiendo del tamaño inicial, provocando que el código empezara a ser poco mantenible dado que existen diferentes dispositivos y el desarrollo crecía de forma lineal con el número de estos.

\medskip

Tras aprender más del lenguaje de desarrollo \textit{Swift} se descubrieron nuevas formas de posicionar los elementos de la interfaz para poder refactorizar el código. A partir de entonces se implementan \textit{constraints}.

\medskip

Las \textit{constraints} son condiciones que se aplican sobre la vista y que son relativas a elementos de la misma. Estas condiciones cumplen operadores relacionales (igualdad, mayor que, menor que... etc) y son gestionadas y actualizadas en todo momento por el sistema. Por lo tanto su uso hace que el código se reduzca y sea más mantenible.

\medskip

En la siguiente figura se muestran las diferentes constraints en color naranja, que son insertadas en los elementos del menú para hacerle saber al sistema dónde se tienen que ubicar.

\begin{adjustbox}{center,caption={Constraints de los elementos del menú},nofloat=figure}
	% This is a non-float method. Look more here https://tex.stackexchange.com/questions/8625/force-figure-placement-in-text.
	\includegraphics[width=0.7\textwidth, height=\textheight, keepaspectratio]{MathType_Constraints/Menu_Constraints.png}
\end{adjustbox}

\subsection{Conexión con los servicios de \textit{MathType}}

Para la implementación se utilizó al principio un proyecto realizado en el pasado por el estudiante del TFG, que consistía en un framework mediante el cual poder tener los servicios del editor \textit{MathType} en offline para \textit{iOS}. De esta manera se quería poder eliminar la dependencia de Internet. Sin embargo, en un análisis posterior se vio la necesidad de utilizar los servicios externos de \textit{MathType}, por lo que se prefirió no usar dicho framework y realizar todo online para así que la aplicación tuviera menos peso y siempre estuviera actualizada.

\medskip

Sin embargo, la conexión con los servicios online plantean dos problemas: existe una seguridad en el editor online que no permite usarlo en un entorno que no sea un servidor web, por lo tanto no es posible cargar un archivo html en local desde el \textit{WebView} y hay que evadir dicha seguridad; por otro lado y debido a que algunos de los servicios que utiliza \textit{MathType for iOS} aún están en fase de desarrollo, hay un problema con la seguridad CORS\cite{CORSW3} que también hay que solucionar.

\medskip

Para el primer problema, se utiliza un servidor que actúa como \textit{bypass} para obtener un archivo html con una dirección aceptada por el editor. Una vez esto, se borra el contenido que hubiera podido quedar antes de ser mostrado en el \textit{WebView} y se carga el editor \textit{MathType Web}. De esta forma, el editor detecta que está siendo ejecutado en un entorno autorizado y permite ser utilizado.

\medskip

Para el segundo problema se realiza un proceso similar mediante otro \textit{bypass}. Para acceder a los servicios de terceros es necesario que el servidor en cuestión nos retorne en la cabecera un argumento mediante el cual autorizarnos a consumir dicho contenido (si no, saltaría un error de CORS). Por lo que se utiliza un servidor externo al cual realizar la petición. Este servidor a su vez redirige la petición al servidor que queremos acceder, recoge la respuesta, le añade las cabeceras necesarias (access-control-allow-origin) y devuelve la respuesta a \textit{MathType for iOS}.

\medskip

Finalmente, hay que recordar que estas dos decisiones son consideradas malas prácticas. Sin embargo, debido a que es fácil refactorizar el código y el estado actual de desarrollo de algunos de los servicios no se ha encontrado otra opción mejor.

\subsection{Importar fórmulas}

Como se comentaba en la sección de diseño, existen limitaciones en \textit{iOS} que afectan a la hora de importar las imágenes generadas.

\medskip

El proceso interno de crear una imagen que ejecuta \textit{MathType} se resume en los siguientes pasos:

\begin{enumerate}
	\item \textbf{Crear la fórmula con las herramientas que proporciona el editor.} Esto lo que hace es mostrar de manera visual al usuario la fórmula, mientras que por debajo se encuentra una representación en formato \textit{MathML} de dicha fórmula.
	\item \textbf{Creación de la imagen de la fórmula.} Se crea la imagen a partir del MathML anterior.
	\item \textbf{Inserción de atributos de estilo.} Se pueden añadir atributos css para el tipo de fuente, altura, anchura... etc.
	\item \textbf{Inserción de atributos de control.} Se añaden atributos que permiten tener control de la fórmula. Por el momento únicamente se utilizan dos: \textit{data-custom-editor}, que permite añadir información sobre la toolbar que se quiere mostrar al usuario durante la reedición de la fórmula; y \textit{data-mathml}, que permite mantener la información del \textit{MathML} que representa la fórmula.
\end{enumerate}

\medskip

El último paso, concretamente el añadir el atributo \textit{data-mathml}, es el que se buscó implementar en \textit{MathType for iOS}. Sin embargo, \textit{iOS} consta de una limitación en el \textit{WebView} que no se tuvo en cuenta.

\medskip

El elemento \textit{WebView} tiene implementado un comportamiento predeterminado para los eventos Drag\&Drop, pero dicho comportamiento provoca que puedan exportarse las imágenes a otras aplicaciones utilizando la clase \textit{Imagen} del sistema. Esta acción a su vez causa que se pierdan los atributos insertados en la imagen y por lo tanto no es posible saber a qué \textit{MathML} hace referencia dicha imagen.

\medskip

Debido a esto no es posible importar fórmulas directamente a \textit{MathType for iOS}. Pero pese a ello, se ha querido aportar una forma alternativa.

\medskip

Para ello se ha utilizado el servicio de reconocimiento de fórmulas de \textit{MathType}. Las imágenes que son llevadas a \textit{MathType for iOS} son capturadas y enviadas a dicho servicio para así, una vez con información de la fórmula que se ha detectado, poder importarse. El uso de este servicio pese a que suele funcionar bien no es del todo correcto, por lo que algunas fórmulas no son importadas totalmente y contienen fallos. Además, la llamada a un servicio externo supone un aumento del tiempo de respuesta de cara al usuario. Por otra parte, esto permite no sólo editar fórmulas creadas por \textit{MathType}, sino que también fórmulas de imágenes que provengan de otras fuentes.

\subsection{Imágenes pesadas}

En cuanto al uso del sistema externo de reconocimiento de fórmulas, se ha realizado un reescalado de las imágenes que se envían.

\medskip

Actualmente los distintos dispositivos que ejecutan el sistema \textit{iOS} pueden llegar a realizar fotografías con una calidad de hasta 4k. Esto en cuanto a la visualización es una gran mejora ya que se obtiene más detalles en la imagen, sin embargo a la hora de realizar la llamada al sistema externo ocurren dos problemas: se tarda más en realizar la petición y el buffer de información puede llegar a desbordarse provocando un error en el sistema.

\medskip

Para evitar este tipo de problemas, las imágenes son reescaladas si superan el tamaño de 400 píxeles de anchura hasta este tamaño. Durante el escalado se rebaja la calidad de imagen y por lo tanto el tamaño de ésta queda reducido, evitando los posibles errores comentados anteriormente.

\subsection{Referencias débiles}

Durante la implementación se ha tenido que tratar numerosos errores como el de las referencias débiles comentados en el diseño de \textit{MathType for iOS}.

\medskip

Como se ve en el esquema conceptual, hay clases como \textit{ConfigurationOption} que son enumerations que implementan el patrón factoría. Sin embargo, después de un tiempo de debug de la aplicación se ha hecho evidente de que iOS utiliza referencia débiles en las vistas que instancia dicho enumeration. Esto afecta de forma directa al comportamiento de la aplicación ya que cuando una vista que está siendo mostrada es liberada de memoria su comportamiento es imprevisible, aunque en la mayoría de casos tiende a no aparecer o a aparecer con ciertos errores de visualización.

\medskip

Para evitar los problemas de dicha referencia débil, se ha creado una estructura que guarda en memoria referencias fuertes de las vistas creadas por dicha factoría.

\subsection{Bugs de iOS}

En la implementación de \textit{MathType for iOS} se han encontrado bugs en distintos elementos de la interfaz. Estos han afectado principalmente al diálogo para elegir entre si se quiere hacer una fotografía o elegir una imagen tomada anteriormente, para reconocer en ella una fórmula.

\medskip

Parece ser que es algo reportado a \textit{Apple} pero que aún no tiene solución oficial. Dicho diálogo permite ser mostrado con estilos distintos, desde un pop-up normal a una ventana que se extiende desde la parte inferior de la pantalla. Sin embargo esta última provoca un error en las \textit{constraints} presentes. La solución ha sido no utilizar el último estilo comentado y mostrar el pop-up normal.

\subsection{Cambio de formato de imagen}

Durante el diseño no se tuvo en consideración si el sistema era capaz de utilizar diferentes formatos de imagen. Esto ha provocado que a la hora de implementar dicha feature se haya descubierto que \textit{iOS} no permite el uso de su clase \textit{Imagen} en formato SVG como se quería, por lo que esta feature queda descartada.

\medskip

El formato SVG proporciona una mejor calidad en las fórmulas renderizadas que el formato PNG. Sin embargo, para minimizar el impacto se ha decidido aumentar la calidad de las imágenes PNG de 90dpi a 400dpi, ya que después de varias pruebas se ha visto que el impacto en el tiempo de respuesta de la aplicación no es apreciable por el usuario.

\section{Automatización de procesos}

\subsection{Herramientas utilizadas}

Actualmente, \textit{Maths for More S.L.} se encuentra realizando un esfuerzo para mejorar la mantenibilidad de sus proyectos respecto al proceso de distribución. Dicho esfuerzo se basa en invertir recursos en la automatización de algunas de las etapas, como por ejemplo la generación de los paquetes software finales que serán distribuidos a los clientes y también la verificación de la calidad o \textit{testing}. Además de aprovechar todo ello para configurar un entorno donde poder aplicar el concepto de \textit{integración continua}\cite{ArticleCI}.

\medskip

Se entiende como \textit{integración} la compilación y ejecución de tests para comprobar el buen funcionamiento del proyecto. Y de este concepto nace la idea de \textit{integración continua} como el acto de hacer integraciones automatizadas de forma periódica, normalmente cuando hay un cambio en el proyecto. De esta manera, es posible detectar posibles fallos rápidamente.

\medskip

\textit{MathType for iOS} no es la excepción y se le pide el mismo nivel de compromiso para mejorar la mantenibilidad, intentando utilizar las herramientas que ya se utilizan en los demás proyectos internos.

\medskip

Por lo tanto, el estudiante del \textit{TFG} tiene que aprender a utilizar dichas herramientas. Cada una tiene una finalidad distinta dependiendo de la etapa que se quiera automatizar y son las siguientes:

\begin{itemize}
	\item \textbf{Git\cite{GitBook}.} Existen varios motivos para utilizar esta herramienta. Primero de todo permite llevar un historial de los cambios en el código, por lo que resulta sencillo deshacer cambios y llevar un control de quién ha modificado algo. Por otra parte, es utilizado ampliamente haciendo uso de un servidor interno o externo donde se suben los cambios que puedan haberse hecho de manera local, para así propagarlos a todas las demás personas que trabajen con el mismo repositorio.
	\item \textbf{Apache Ant\cite{AntBook}.} Esta es una herramienta de automatización de procesos de compilación. A pesar de haber más y mejores en el mercado se ha escogido debido a que los demás proyectos de \textit{Maths for More S.L.} la utilizan.
	\item \textbf{Appium\cite{AppiumWebsite}.} Framework open source para la automatización de tests en aplicaciones móviles. Esta es la única herramienta nueva que utiliza este proyecto respecto a los demás de \textit{Maths for More S.L.}. Esto se debe a que es la primera aplicación móvil y no se cuenta con ninguna herramienta previa.
	\item \textbf{Jenkins\cite{JenkinsWebsite}.} Esta herramienta es un servidor que permite ser configurado para implementar el modelo de \textit{integración contínua} explicado anteriormente.
\end{itemize}

\subsection{Automatización de la compilación}

Una de las partes a automatizar de un proyecto es la compilación. Esto ayuda a generar los paquetes que podrán ser distribuidos a los clientes con facilidad. Además, ayuda al mantenimiento de un proyecto gracias a que el \textit{script} de automatizado hace que quede constancia del cómo hay que hacerse, por lo que en caso de que la persona que sabía compilar el proyecto se marche, éste podrá ser continuado por otra sin que la primera haya tenido que dejar documentación previa.

\begin{adjustbox}{center,caption={Logo de Apache Ant.},nofloat=figure}
	% This is a non-float method. Look more here https://tex.stackexchange.com/questions/8625/force-figure-placement-in-text.
	\includegraphics[width=0.5\textwidth, height=\textheight, keepaspectratio]{apache-ant-logo.png}
\end{adjustbox}

Como se comentó anteriormente, \textit{Apache Ant} ayuda a la automatización de procesos de compilación, por lo que será utilizada para este fin. Dicha herramienta tiene multitud de acciones genéricas mediante las cuales puede llevarse a cabo esta tarea: crear directorios, ejecutar scripts por línea de comandos, copiar archivos... etc.

\medskip

En el caso de este proyecto, la automatización se ha realizado a nivel de proyecto de \textit{XCode}, la interfaz de desarrollo que proporciona \textit{Apple} para programar en \textit{iOS}.

\medskip

Cuando se dice \enquote*{nivel de proyecto} se refiere a que no existen recursos comunes a otros proyectos internos de \textit{Maths for More S.L.}. Ya que, en caso de que hubieran recursos comunes, como por ejemplo imágenes corporativas susceptibles de cambios, habría que mantener dichos recursos a parte y que los proyectos que los usaran los copiaran. De esta forma, en caso de cambio de un recurso, bastaría con hacer un cambio en el recurso y todos los proyectos que dependen de él descargarían el nuevo con los cambios.

\medskip

Entonces, para automatizar el proyecto, primero ha habido un periodo de autoaprendizaje de \textit{Apache Ant} y las herramientas de desarrollo de \textit{XCode}, además de la lectura de documentación para saber qué acciones pueden llevar a cabo. Después de ello, se ha creado en la raíz del proyecto un archivo \textit{build.xml} en el cual, utilizando lenguaje \textit{XML} se pueden definir las tareas que se quieren ejecutar en \textit{Apache Ant}, como el que se puede ver a continuación.

\medskip

\begin{adjustbox}{center,caption={Documento \textit{XML} de Apache Ant.},nofloat=figure}
	% This is a non-float method. Look more here https://tex.stackexchange.com/questions/8625/force-figure-placement-in-text.
	\includegraphics[width=\textwidth, height=\textheight, keepaspectratio]{Ant/ant-build.png}
\end{adjustbox}

En el caso de este \textit{TFG}, las tareas han consistido en usar el entorno de líneas de comandos de \textit{XCode} y crear las propiedades necesarias, como la localización del paquete resultante, para facilitar el desarrollo durante la automatización de tests.

\medskip

Una vez terminado, el proyecto está listo para compilarse con una línea de código: \textit{ant build}.

\subsection{Automatización de los tests}

En cuanto a la automatización de tests, va íntimamente ligada a la automatización de la compilación, ya que depende de que exista un paquete previo donde poder ejecutarse.

\begin{adjustbox}{center,caption={Logo de Appium.},nofloat=figure}
	% This is a non-float method. Look more here https://tex.stackexchange.com/questions/8625/force-figure-placement-in-text.
	\includegraphics[width=0.5\textwidth, height=\textheight, keepaspectratio]{appium-logo.png}
\end{adjustbox}

Como se explicaba al principio, se ha escogido \textit{Appium} como herramienta de automatización de tests, ésta requiere de dos servicios propios corriendo en la máquina destinada al \textit{testeo}, pero que se ha obviado su automatización dado que forma parte de la configuración de cada máquina y una vez configurados no hay que volver a hacer nada.

\medskip

Dicha herramienta aporta un framework implementado en el lenguaje \textit{Java} donde se permiten crear tests de \textit{JUnit}\cite{JUnitWebsite}. Además, como se puede ver en la siguiente figura, tiene un \textit{inspector} para mejorar la tarea de \textit{debug} de la aplicación y también la propia creación de tests, ya que permite entre otras cosas crear comandos sobre los elementos de la interfaz.

\begin{adjustbox}{center,caption={Webinspector de Appium.},nofloat=figure}
	% This is a non-float method. Look more here https://tex.stackexchange.com/questions/8625/force-figure-placement-in-text.
	\includegraphics[width=\textwidth, height=\textheight, keepaspectratio]{Appium/appium-webinspector.png}
\end{adjustbox}

\medskip

Por este motivo, el archivo \textit{build.xml} comentado en la sección anterior ha sido ampliado para añadir una nueva tarea que se ejecute con un \textit{ant test}, que pasa los tests. Esta tarea no está incluida en el comando \textit{ant build} comentado anteriormente para que, como desarrollador, sea posible elegir hace lo uno o lo otro.

\medskip

Llegados aquí, el archivo \textit{build.xml} tiene los dos comandos: \textit{ant build} y \textit{ant test}, que además pueden ejecutarse uno detrás de otro con un \textit{ant build test}.

\begin{adjustbox}{center,caption={Test de JUnit en Appium.},nofloat=figure}
	% This is a non-float method. Look more here https://tex.stackexchange.com/questions/8625/force-figure-placement-in-text.
	\includegraphics[width=\textwidth, height=\textheight, keepaspectratio]{Appium/appium-test.png}
\end{adjustbox}

Como comentarios adicionales, cabe destacar que para la creación de las tareas, se ha decidido mantener dos repositorios para el proyecto de \textit{MathType for iOS}: uno contiene el desarrollo de la aplicación y otro el sistema de \textit{testing}. Cuando el desarrollador realiza un \textit{ant test} se descarga todo el sistema de tests y se ejecutan para comprobar la calidad del software. De esta manera, siempre se mantienen actualizados todos los tests.

\subsection{Integración contínua}

\begin{adjustbox}{center,caption={Logo de Jenkins.},nofloat=figure}
	% This is a non-float method. Look more here https://tex.stackexchange.com/questions/8625/force-figure-placement-in-text.
	\includegraphics[width=0.5\textwidth, height=\textheight, keepaspectratio]{jenkins-logo.png}
\end{adjustbox}

Para terminar, se hablará de la última parte de la automatización, la máquina que ejecutará el proceso de \textit{integración contínua}. Para ello, se utiliza la herramienta \textit{Jenkins}.

\medskip

Jenkins es una aplicación que se ejecuta en un servidor. Ésta ofrece un servicio de \enquote*{nodos}, que son máquinas (a veces también llamadas \textit{slaves}) que pueden conectarse a dicho servidor para que éste descargue en ellas los repositorios de los proyectos que interesan y se ejecuten las tareas de compilación y testeo. De esta forma, se pueden mantener máquina independientes con características específicas que comprueban el buen estado de todos los proyectos en sus respectivos repositorios de \textit{Git}.

\medskip

Para hacer esto, se ha creado un nuevo archivo en la raíz del proyecto llamado \textit{Jenkinsfile}. Este archivo guarda la configuración que debe tener respecto al proyecto el servidor Jenkins cuando éste quiera hacer pruebas del repositorio en cuestión.

\medskip

En este archivo entonces, se ha definido dos cosas principalmente: el nodo (máquina) en el cual tiene que ejecutarse la compilación y testeo; y las fases (o \textit{stages}) que se llevarán a cabo.

\medskip

En el caso de \textit{MathType for iOS} solamente hay dos fases: \textit{Build} y \textit{Test}. En cada una se ejecutan los comandos análogos de ant definidos previamente. Sin embargo, en un futuro se espera aumentar dichas fases para añadir pasos extra como la automatización que publique directamente el paquete en la \textit{AppStore} de \textit{Apple} cuando los procesos hayan finalizado con éxito.

\medskip

Una vez terminado todo este trabajo de configuración, el resultado se puede ver en la siguiente figura. Se muestran cada una de las pruebas que se han pasado.

\medskip

Además, dichas pruebas han sido configuradas a conciencia para que sean ejecutadas cada vez que existe un cambio en el repositorio de \textit{Git} principal. Por lo que con cada cambio de código se podrán tener avisos de si existen o no errores.

\begin{adjustbox}{center,caption={Resultados de los tests de \textit{MathType for iOS} en \textit{Jenkins}.},nofloat=figure}
	% This is a non-float method. Look more here https://tex.stackexchange.com/questions/8625/force-figure-placement-in-text.
	\includegraphics[width=\textwidth, height=\textheight, keepaspectratio]{Jenkins/jenkins2.png}
\end{adjustbox}

\subsection{Diagrama de desarrollo}

Terminando de explicar, falta detallar el diagrama de desarrollo que representa la puesta en producción.

\medskip

Cuando se habla de diagrama de desarrollo se hace referencia a dónde se ejecuta cada pieza del conjunto software, ya que puede ser contenido en una o en diferentes máquinas que se comunican entre sí.

\medskip

En el caso de \textit{MathType for iOS} se utiliza una arquitectura a la que se le ha dado el nombre de \textit{ASA (Aplicación, Servicios y Automatización)}, debido a que el código se ejecuta en las primeras dos partes, mientras que a la tercera se la hace responsable de la automatización del paquete a distribuir y el \textit{testing} del mismo.

\medskip

En la siguiente figura, se puede observar el diagrama de desarrollo del sistema.

\vspace{5mm}

\begin{adjustbox}{center,caption={Diagrama de desarrollo de \textit{MathType for iOS}.},nofloat=figure}
	% This is a non-float method. Look more here https://tex.stackexchange.com/questions/8625/force-figure-placement-in-text.
	\includegraphics[width=\textwidth, height=0.95\textheight, keepaspectratio]{Diagrama_Fisico/Diagrama_Desarrollo.png}
\end{adjustbox}

\paragraph{Aplicación}

La aplicación es el elemento en el cual se ejecuta el programa. En el caso de \textit{MathType for iOS} se hablaría del dispositivo \textit{iOS} con la aplicación instalada que es abierta por el usuario.

\medskip

Dicho elemento contiene la aplicación \textit{MathType for iOS} y es el responsable directo de la interacción con el usuario que quiere utilizar el sistema. 

\paragraph{Servicios}

Sin embargo existe parte de la lógica del sistema, como el reconocimiento de una fórmula a partir de una imagen, que no es posible que se ejecute en el propio dispositivo debido a que se escapa del dominio del TFG. Entonces, para suplir esta necesidad se utilizan servicios externos ya creados por \textit{Maths for More S.L.}.

\medskip

Estos servicios externos son ejecutados en el servidor de la empresa  y realizan de forma independiente el trabajo de cómputo del resultado, que será devuelto a la aplicación.

\paragraph{Automatización}

Finalmente, debido a que una de las metas que se persiguen es la entrega de un software de calidad, debe existir un elemento físico del sistema que se encargue de realizar las tareas de comprobación de \textit{MathType for iOS}.

\medskip

Para ello, dicho elemento físico genera de manera automática los paquetes que se distribuirán y a su vez genera contextos análogos a los que se tienen en producción. En estos contextos se ejecuta una simulación de un dispositivo con el sistema \textit{iOS} y se pasan los tests explicados anteriormente con la intención de comprobar la ausencia de errores.

